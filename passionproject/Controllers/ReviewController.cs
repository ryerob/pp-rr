﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using passionproject.Models;
using System.Diagnostics;
using passionproject.Models.ViewModels;


namespace passionproject.Controllers
{
    public class ReviewController : Controller
    {
        private BeerCMSContext db = new BeerCMSContext();
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult List()
        {
            return View(db.Reviews.ToList());
        }
        public ActionResult New()
        {
            return View(db.Beers.ToList());
        }
        [HttpPost]
        public ActionResult Create(int? id, string ReviewTitle_New, string ReviewerName_New, string ReviewContent_New, int? ReviewBeer_New)
        {
            string DBquery = "insert into Reviews (ReviewTitle, ReviewerName, ReviewContent, beer_BeerID)" +
            "values (@Title,@Name,@Content,@bid)";

            SqlParameter[] reviewparams = new SqlParameter[4];
            reviewparams[0] = new SqlParameter("@Title", ReviewTitle_New);
            reviewparams[1] = new SqlParameter("@Name", ReviewerName_New);
            reviewparams[2] = new SqlParameter("@Content", ReviewContent_New);
            reviewparams[3] = new SqlParameter("@bid", ReviewBeer_New);

            db.Database.ExecuteSqlCommand(DBquery, reviewparams);
            Debug.WriteLine(DBquery);

            return RedirectToAction("Details/"+ReviewBeer_New, "Beer");
        }
        [HttpPost]
        public ActionResult Edit(int? id, string ReviewTitle, string ReviewerName, string ReviewContent, int? ReviewBeer)
        {
            string DBquery = "update Reviews set ReviewTitle=@Title, ReviewerName=@Name, ReviewContent=@Content where ReviewID=@id";
            SqlParameter[] reviewparams = new SqlParameter[4];
            reviewparams[0] = new SqlParameter("@Title", ReviewTitle);
            reviewparams[1] = new SqlParameter("@Name", ReviewerName);
            reviewparams[2] = new SqlParameter("@Content", ReviewContent);
            reviewparams[3] = new SqlParameter("@bid", ReviewBeer);


            db.Database.ExecuteSqlCommand(DBquery, reviewparams);

            return RedirectToAction("List/");
        }
        public ActionResult Delete(int? id)
        {
            Review review = db.Reviews.Find(id);
            if (review == null)
            {
                return HttpNotFound();
            }
            return View(review);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Review review = db.Reviews.Find(id);
            db.Reviews.Remove(review);
            db.SaveChanges();
            return RedirectToAction("List", "Beer");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }   
    }
}