﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.SqlClient;
using passionproject.Models;
using passionproject.Models.ViewModels;
using System.Data.Entity;
using System.Net;
using System.Diagnostics;

namespace passionproject.Controllers
{
    public class BeerController : Controller
    {
        private BeerCMSContext db = new BeerCMSContext();
        // GET: Beer
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        //main list of beer with links
        public ActionResult List()
        {
            IEnumerable<Beer> beer = db.Beers.ToList();
            return View(beer);
        }
        public ActionResult New()
        {
            return View();
        }
        //when create new is clicked
        [HttpPost]
        public ActionResult Create(string BeerName_New, string BeerDescription_New)
        {
            string DBquery = "insert into Beers (BeerName, BeerDescription) values (@name, @description)";
            SqlParameter[] createparams = new SqlParameter[2];
            createparams[0] = new SqlParameter("@Name", BeerName_New);
            createparams[1] = new SqlParameter("@Description", BeerDescription_New);

            db.Database.ExecuteSqlCommand(DBquery, createparams);
            return RedirectToAction("List");
        }
        //when edit is clicked
        [HttpPost]
        public ActionResult Edit(int id, string BeerName, string BeerDescription)
        {
            string DBquery = "update Beers set BeerName=@Name, BeerDescription=@Description where BeerID=@id";
            SqlParameter[] editparams = new SqlParameter[3];
            editparams[0] = new SqlParameter("@Name", BeerName);
            editparams[1] = new SqlParameter("@Description", BeerDescription);
            editparams[2] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(DBquery, editparams);

            return RedirectToAction("Details/" + id);
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Beer beer = db.Beers.Find(id);
            if (beer == null)
            {
                return HttpNotFound();
            }
            return View(beer);
        }
        //when details is clicked
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Beer beer = db.Beers.Find(id);
            if ( beer == null)
            {
                return HttpNotFound();
            }
            return View(beer);
        }
        public ActionResult Show(int? id)
        {
            if ((id == null) || (db.Beers.Find(id) == null))
            {
                return HttpNotFound();
            }
            string DBquery = "select * from beers where beerid=@id";
            SqlParameter[] showparams = new SqlParameter[1];
            showparams[0] = new SqlParameter("@id", id);

            Beer allbeer = db.Beers.SqlQuery(DBquery, showparams).FirstOrDefault();

            return View(allbeer);
        }
        //when delete is clicked
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Beer beer = db.Beers.Find(id);
            if (beer == null)
            {
                return HttpNotFound();
            }
            return View(beer);
        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Beer beer = db.Beers.Find(id);
            db.Beers.Remove(beer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}