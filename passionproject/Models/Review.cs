﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace passionproject.Models
{
    public class Review
    {
        [Key]
        public int ReviewID { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string ReviewTitle { get; set; }

        [Required, StringLength(50), Display(Name = "Name")]
        public string ReviewerName { get; set; }

        [Required, StringLength(int.MaxValue), Display(Name = "Content")]
        public string ReviewContent { get; set; }

        public virtual Beer Beer { get; set; }
    }
}