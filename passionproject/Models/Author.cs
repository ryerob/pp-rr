﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace passionproject.Models
{
    public class Author
    {
        [Key]
        public int AuthorID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string AuthorFName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string AuthorLName { get; set; }

        [StringLength(255), Display(Name = "Username")]
        public string AuthorUsername { get; set; }

        //many reviews to one author
        public virtual ICollection<Review> Reviews { get; set; }
    }
}