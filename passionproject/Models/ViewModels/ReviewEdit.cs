﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace passionproject.Models.ViewModels
{
    public class ReviewEdit
    {
        public ReviewEdit()
        {

        }

        public virtual Review review { get; set; }

        public IEnumerable<Beer> beers { get; set; }
    }
}