﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace passionproject.Models
{
    public class BeerCMSContext : DbContext
    {

        public BeerCMSContext()
        {

        }
       
        public DbSet<Beer> Beers { get; set; }
        public DbSet<Review> Reviews { get; set; }
    }
}